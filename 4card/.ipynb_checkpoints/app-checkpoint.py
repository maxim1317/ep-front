"""Summary

Attributes:
    app (TYPE): Description
"""
# import time
# import datetime
from flask import Flask, send_from_directory, render_template


    # def stream_template(template_name, **context):
    #     app.update_template_context(context)
    #     t = app.jinja_env.get_template(template_name)
    #     rv = t.stream(context)
    #     rv.disable_buffering()
    #     return rv


app = Flask(__name__)


@app.route('/', methods=['GET'])
def index():
    # Video streaming home page.
    return render_template('index.html', people_1=1, warning_1="Нарушений нет")


@app.route('/assets/<path:path>')
def send_assets(path):
    return send_from_directory('assets', path)


# def generate():
#     while True:
#         yield "{}".format(datetime.now().isoformat())
#         time.sleep(1)


# @app.route('/')
# def doyouhavethetime():
#     tm = generate()
#     return Response(stream_with_context(stream_template('index.html', people_1=tm, warning_1='?')))


def main():
    app.run(host='0.0.0.0', port=3030, threaded=True, debug=True)


if __name__ == '__main__':
    main()
